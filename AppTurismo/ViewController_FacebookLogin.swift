//
//  ViewController_FacebookLogin.swift
//  AppTurismo
//
//  Created by ANDRES DE LA TORRE on 7/8/17.
//  Copyright © 2017 Andrés de la Torre. All rights reserved.
//

import UIKit
import FacebookLogin
import FBSDKLoginKit


class ViewController_FacebookLogin: UIViewController, FBSDKLoginButtonDelegate {

    let loginButton = FBSDKLoginButton()
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        contador = 0;
        print("Sesión Terminada")
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error == nil{
             print("Se ha iniciado sesión con Facebook")
            contador+=1
             self.performSegue(withIdentifier: "segue1", sender: self)
        }else{
            print(error.localizedDescription)
        }
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        loginButton.center = view.center
        
        view.addSubview(loginButton)
        
        loginButton.delegate = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if ((FBSDKAccessToken.current()) != nil) && (contador == 0) {
            // User is logged in, use 'accessToken' here.
            //prepare(for: UIStoryboardSegue, sender: Any?)
            print("ya estas logeado")
            
            self.performSegue(withIdentifier: "segue1", sender: self)
            print("suegeueee")
            //contador+=1
            print("contador = \(contador)")
            
            
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        var destinationVC:TabBarController = segue.destination as! TabBarController
//        
//    }
    
//    override func performSegue(withIdentifier identifier: "segue1", sender: nil ) {
//        print("YA")
//    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
