//
//  ViewController_Map.swift
//  AppTurismo
//
//  Created by ANDRES DE LA TORRE on 3/8/17.
//  Copyright © 2017 Andrés de la Torre. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewController_Map: UIViewController, GMSMapViewDelegate {
    
    var locationManager = CLLocationManager()
    var mapView:GMSMapView!    
    
    @IBOutlet weak var googleMapView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        addMap()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func addMap() {
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 5.0)
        //mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView = GMSMapView.map(withFrame: googleMapView.bounds, camera: camera)
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        //view = mapView
        googleMapView.addSubview(mapView)
        
        // Creates a marker in the center of the map.
        /*let marker = GMSMarker()
         marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
         marker.title = "Sydney"
         marker.snippet = "Australia"
         marker.map = mapView*/
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        locationManager.requestWhenInUseAuthorization()
        let location = locationManager.location?.coordinate
        let camera = GMSCameraPosition.camera(withLatitude: (location?.latitude)!, longitude: (location?.longitude)!, zoom: 15.0)
        mapView.animate(to: camera)
        
        return true
    }
    
    @IBAction func hibridoButtonPressed(_ sender: Any) {
        mapView.mapType = .hybrid
    }
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        let position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        let marker = GMSMarker(position:position)
        
        let destination = CLLocation(latitude:coordinate.latitude, longitude:coordinate.longitude)
        let locValue = locationManager.location!
        let distance = destination.distance(from: locValue)
        
        marker.title = "Mi marcador"
        marker.snippet = String(distance)
        marker.map = mapView
        
        
    }

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
