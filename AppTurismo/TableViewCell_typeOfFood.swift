//
//  TableViewCell_typeOfFood.swift
//  AppTurismo
//
//  Created by ANDRES DE LA TORRE on 29/7/17.
//  Copyright © 2017 Andrés de la Torre. All rights reserved.
//

import UIKit

class TableViewCell_typeOfFood: UITableViewCell {
    
    
    @IBOutlet weak var label_restaurant: UILabel!
    @IBOutlet weak var image_restaurant: UIImageView!
    @IBOutlet weak var label_Distance: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
