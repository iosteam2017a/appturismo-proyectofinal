//
//  TabBarController.swift
//  AppTurismo
//
//  Created by Juan Erazo on 7/27/17.
//  Copyright © 2017 Andrés de la Torre. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        let items = tabBar.items
        //items?[0].badgeValue = "3"
        //items?[0].badgeColor = UIColor(red: 0/255.0, green: 127/255.0, blue: 255/255.0, alpha: 1)
        //alpha es la transparencia
        items?[0].title = "Comida"
        items?[0].image = #imageLiteral(resourceName: "food")
        
        //items?[1].badgeValue = "3"
        //items?[1].badgeColor = UIColor(red: 32/255.0, green: 143/255.0, blue: 200/255.0, alpha: 1)
        //alpha es la transparencia
        items?[1].title = "Eventos"
        items?[1].image = #imageLiteral(resourceName: "events")
        
        //tabBar.tintColor = UIColor(red: 23/255.0, green: 234/255.0, blue: 122/255.0, alpha: 1)
        
        //Settings
        items?[2].title = "Configuración"
    }

}
