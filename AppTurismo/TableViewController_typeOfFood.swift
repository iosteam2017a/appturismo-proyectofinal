//
//  TableViewController_typeOfFood.swift
//  AppTurismo
//
//  Created by ANDRES DE LA TORRE on 29/7/17.
//  Copyright © 2017 Andrés de la Torre. All rights reserved.
//
// --> https://www.youtube.com/watch?v=k7_XFDXiGfU

import UIKit

class TableViewController_typeOfFood: UITableViewController, UISearchBarDelegate {
    
    var listTypeOfFood = ["COSTEÑA", "SERRANA", "BLA - BLA"]
    var listRestaurant1 = ["1","2","3"]
    var listRestaurant2 = ["4","5","6"]
    var listRestaurant3 = ["7","8","7"]
    
    var listDistanceRestaurant1 = ["1 km","2 km","3 km"]
    var listDistanceRestaurant2 = ["4 km","5 km","6 km"]
    var listDistanceRestaurant3 = ["7 km","8 km","7 km"]
    
    var sectionData: [Int: [String]] = [:]
    //let sectionImage [UIImage] = []
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        createSearchBar()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        sectionData = [0: listRestaurant1, 1: listRestaurant2, 2: listRestaurant3]

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
    }
    
    func createSearchBar(){
        let searchBar = UISearchBar()
        searchBar.showsCancelButton = true
        searchBar.placeholder = "Escriba su búsqueda aquí"
        searchBar.delegate = self
        
        self.navigationItem.titleView = searchBar
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return listTypeOfFood.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return listTypeOfFood[section]
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (sectionData[section]?.count)!
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellTypeOfFood", for: indexPath) as! TableViewCell_typeOfFood


        cell.label_restaurant.text = sectionData[indexPath.section]![indexPath.row]
        //cell.label_TypeOfFood.text = listTypeOfFood[indexPath.row]
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
